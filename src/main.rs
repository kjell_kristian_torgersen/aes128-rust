use aes128::AES128;

mod aes128;
mod aes_lut;
use std::time::Instant;

fn show_array(name: &str, array: &[u8]) {
    print!("{}", name);
    for i in 0..array.len() {
        print!("{:02x} ", array[i]);
    }
    println!();
}

fn process(key: &[u8; 16], nonce: &[u8; 8], dst: &mut [u8], src: &[u8]) {
    let mut aes128 = AES128::new(&key);
    let blocks = src.len() / 16;
    let mut tmp = [0; 16];
    let rest = src.len() % 16;
    let mut iv: [u8; 16] = [
        nonce[0], nonce[1], nonce[2], nonce[3], nonce[4], nonce[5], nonce[6], nonce[7], 0, 0, 0, 0,
        0, 0, 0, 0,
    ];
    for i in 0..blocks {
        for j in 0..8 {
            iv[8 + j] = (i >> (8 * j)) as u8;
        }
        aes128.encrypt(&mut tmp, &iv);
        for j in 0..16 {
            dst[16 * i + j] = tmp[j] ^ src[16 * i + j];
        }
    }
    if rest != 0 {
        for j in 0..8 {
            iv[8 + j] = (blocks >> (8 * j)) as u8;
        }
        aes128.encrypt(&mut tmp, &iv);
        for j in 0..rest {
            dst[16 * blocks + j] = tmp[j] ^ src[16 * blocks + j];
        }
    }
}

fn test_ctr() {
    let nonce = [0u8; 8];
    let key = [0u8; 16];

    let mut plain1 = [0u8; 10*1024];
    let mut cipher = [0u8; 10*1024];
    //let mut plain2 = [0u8; 10*1024];
    let mut bytes = 0;
    let now = Instant::now();
    while now.elapsed().as_secs() < 5 {
        process(&key, &nonce, &mut cipher, &plain1);
        process(&key, &nonce, &mut plain1, &cipher);
        bytes += 2*10*1024;
    }
    println!("{} bytes in {} sec = {} MB / sec ", bytes, 5, (bytes) as f64 / 5.0 / 1024.0 / 1024.0);

    /*show_array("ctr: plain1: ", &plain1);
    show_array("ctr: cipher: ", &cipher);
    show_array("ctr: plain2: ", &plain2)*/
}

fn main() {
    let key = [0; 16];
    let plain1 = [0; 16];
    let mut plain2 = [0; 16];
    let mut cipher = [0; 16];

    let mut aes128 = aes128::AES128::new(&key);

    aes128.encrypt(&mut cipher, &plain1);
    aes128.decrypt(&mut plain2, &cipher);

    show_array("ecb: plain1: ", &plain1);
    show_array("ecb: cipher: ", &cipher);
    show_array("ecb: plain2: ", &plain2);

    println!();
    test_ctr();
}
